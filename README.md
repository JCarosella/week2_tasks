# week2_tasks

Fake transform from map to base: rosrun tf static_transform_publisher 0 0 0 0 0 0 1 map base 10

Tools to visualize and check urdf: sudo apt-get install liburdfdom-tools
- check_urdf robot.urdf
- create tf tree pdf: urdf_to_graphiz robot.urdf

Basic plugins tutorial from gazebo web: http://gazebosim.org/tutorials?cat=write_plugin

Velodyne tutorial: http://gazebosim.org/tutorials?cat=guided_i&tut=guided_i1
